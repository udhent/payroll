<?php
session_start();

/*
* file yang menerima semua request dari view
* dan di teruskan ke DB_Functions untuk berkomunikasi dengan database
* desember 2014
* author nasa
*/

include "../lib/php/DB_Functions.php";

$postData = file_get_contents('php://input');
$postData = json_decode($postData, true);
date_default_timezone_set('UTC');

$db = new DB_Functions();
$db->connect();

$action = $postData['action'];

switch($action) {
    case "list_karyawan":
        $db->select('karyawan');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "update_karyawan":
        $nik = $postData['nik'];
        $nama = $postData['nama'];
        $telp = $postData['telp'];
        $alamat = $postData['alamat'];
        $msk_krj = $postData['msk_krj'];

        $hasil = $db->update('karyawan', array('nm_karyawan'=>$nama, 'alamat'=>$alamat, 'msk_krj'=>$msk_krj, 'telp'=>$telp), array('nik', $nik));
        if($hasil) {
            echo json_encode(array('pesan'=>'data berhasil di update'));
        } else {
            echo json_encode(array('pesan' => 'gagal'));
        }
        break;

    case "absen":
        $nik = $postData['nik'];
        $db->select('karyawan', '*', "nik='".$nik."'");
        $hasil = $db->getResult();
        if(sizeof($hasil) <= 0) {
            echo json_encode(array('pesan'=>'gagal'));
        } else {
            echo json_encode($hasil);
        }
        $db->disconnect();
        break;

    case "simpan_absen":
        $nik = $postData['nik'];
        //$jns = $postData['jenis'];
        $jml = $postData['jmlh'];
        $jmlJam = $postData['jam'];

        //---- cek bila sudah di absen ----

        $db->select('absensi a inner join pembayaran p on a.nik=p.nik', 'a.nik, a.tgl_absen', "a.tgl_absen >= p.priode_awal and a.tgl_absen <= p.priode_akhir and a.nik='".$nik."'");
        $hasil_cek = $db->getResult();

        if(count($hasil_cek) > 0) {
            echo json_encode(array('pesan' => 'karyawan telah di absen'));
        } else {

            $hasil = $db->insert('absensi', array(0, $nik, $jml, date("Y-m-d")), 'jenis, nik, jml, tgl_absen');
            $hasil = $db->insert('absensi', array(1, $nik, $jmlJam, date("Y-m-d")), 'jenis, nik, jml, tgl_absen');

            if($hasil) {
                echo json_encode(array('pesan'=>'berhasil'));
            } else {
                echo json_encode(array('pesan'=>'Gagal'));
            }

        }
        $db->disconnect();
        break;

    case "select_jabatan_sub":
        $db->select('jabatan');
        $hasil['jabatan'] = $db->getResult();
        $db->disconnect();
        $db->connect();
        $db->select('divisi');
        $hasil['divisi'] = $db->getResult();
        echo json_encode($hasil);
        $db->disconnect();
        break;

    case "tambah_divisi":
        $nm_divisi = $postData['divisi'];
        $db->disconnect();
        $db->connect();
        $insert_divisi = $db->insert('divisi', array($nm_divisi), 'nm_divisi');
        if($insert_divisi) {
            echo json_encode(array('pesan' => 'Berhasil'));
        } else {
            echo json_encode(array('pesan' => 'Gagal'));
        }
        $db->disconnect;
        break;

    case "list_divisi":
        $db->select('divisi');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "detail_divisi":
        $id_divisi = $postData['id'];
        $db->select('divisi', '*', "id='".$id_divisi."'");
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "update_divisi":
        $id_divisi = $postData['id'];
        $divisi = $postData['divisi'];
        $hasil = $db->update('divisi', array('nm_divisi'=>$divisi), array('id', $id_divisi));
        if($hasil) {
            echo json_encode(array('pesan' => 'berhasil'));
        } else {
            echo json_encode(array('pesan' => 'Gagal'));
        }
        $db->disconnect();
        break;

    case "hapus_divisi":
        $id_divisi = $postData['id'];
        $hasil = $db->delete('divisi', "id='".$id_divisi."'");
        if($hasil) {
            echo json_encode(array('pesan' => 'dihapus'));
        } else {
            echo json_encode(array('pesan' => 'gagal'));
        }
        $db->disconnect();
        break;

    case "get_last_nik":
        $db->select('karyawan', 'max(nik) as nik');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "data_lembur":
        $db->select('lembur', '*');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "detail_karyawan":
        $nik = $postData['nik'];
        $db->select('karyawan', '*', "nik='".$nik."'");
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "hapus_lembur":
        $kd_lembur = $postData['kd_lembur'];
        $db->delete('lembur', "kd_lembur='".$kd_lembur."'");
        echo "dihapus ".$kd_lembur;
        echo json_encode($postData);
        $db->disconnect();
        break;

    case "hapus_karyawan";
        $nik = $postData['nik'];
        $hasil = $db->delete('karyawan', "nik='".$nik."'");
        if($hasil) {
            echo "Karyawan dengan nik ".$nik." telah di hapus.. dari php";
        } else {
            echo "Hapus gagal";
        }
        $db->disconnect();
        break;

    case "tambah_set_lembur":
        $kd_lembur = $postData['kd_lembur'];
        $jam_mulai = $postData['jam_mulai'];
        $jam_akhir = $postData['jam_akhir'];
        $f_kali = $postData['f_kali'];
        $sts_hari = $postData['sts_hari'];
        $hasil = $db->insert('lembur', array($kd_lembur, $jam_mulai, $jam_akhir, $f_kali, $sts_hari), 'kd_lembur, jam_mulai, jam_akhir, f_kali, sts_hari');
        if($hasil) {
            echo "Berhasil";
        } else {
            echo "Gagal";
        }
        $db->disconnect();
        break;

    case "list_jabatan":
        $db->select('jabatan, golongan', 'jabatan.*, golongan.nm_gol as golongan', 'jabatan.kd_gol = golongan.kd_gol');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "get_last_jabatan":
        $db->select('jabatan', 'max(kd_jbt) as kode');
        $kode = $db->getResult();
        $db->select('golongan');
        $golongan = $db->getResult();

        echo json_encode(array('kode' => $kode, 'golongan' => $golongan));
        $db->disconnect();
        break;

    case "tambah_jabatan":
        $kd_jbt = $postData['kd_jbt'];
        $jabatan = $postData['jabatan'];
        $kd_gol = $postData['kd_gol'];
        $bobot = $postData['bobot'];

        $db = new DB_Functions();
        $db->connect();

        $hasil = $db->insert('jabatan', array($kd_jbt, $jabatan, $kd_gol, $bobot));
        if($hasil) {
            echo json_encode(array('pesan'=> 'berhasil'));
        } else {
            jason_encode(array('pesan'=>'gagal'));
        }
        $db->disconnect();
        break;

    case "hapus_jabatan":
        $kd_jbt = $postData['kd_jbt'];
        $db->delete('jabatan', "kd_jbt='".$kd_jbt."'");
        $db->disconnect();
        break;

    case "list_komponen":
        $db->select('komponen');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "get_last_komponen":
        $db->select('komponen', 'max(kd_komp) as kode');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "hapus_komponen":
        $kd_komp = $postData['kd_komp'];
        $hasil = $db->delete('komponen', "kd_komp='".$kd_komp."'");
        if($hasil) {
            echo json_encode(array('pesan' => 'Komponen gaji dengan Kode '.$kd_komp.' berhasil di hapus'));
        } else {
            echo json_encode(array('pesan' => 'Gagal menghapus komponen'));
        }
        $db->disconnect();
        break;

    case "tambah_komponen":
        $kd_komp = $postData['kd_komp'];
        $komponen = $postData['komponen'];
        $nilai = $postData['nilai'];
        $ket = $postData['ket'];
        $status = $postData['status'];

        $hasil = $db->insert('komponen', array($kd_komp, $komponen, $ket, $nilai, $status));
        if($hasil) {
            echo json_encode(array('pesan' => 'Komponen Berhasil ditambahkan'));
        } else {
            echo json_encode(array('pesan' => 'Gagal Menambahkan Komponen'));
        }
        $db->disconnect();
        break;

    case "report_karyawan":
        $db->select('karyawan k inner join jabatan j on k.kd_jbt = j.kd_jbt inner join divisi d on k.kd_divisi=d.id', 'k.*, j.nm_jbt, d.nm_divisi');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "daftar_absen_karyawan":
        $db->select('karyawan k inner join jabatan j on k.kd_jbt = j.kd_jbt inner join divisi d on k.kd_divisi = d.id', 'k.*, j.nm_jbt, d.nm_divisi');
        $data_karyawan = $db->getResult();
        $db->disconnect();
        $db = new DB_Functions();
        $db->connect();
        $db->select('absensi', '*', 'jenis=0');
        $krj_normal = $db->getResult();
        $db->disconnect();
        $db = new DB_Functions();
        $db->connect();
        $db->select('absensi', '*', 'jenis=1');
        $lembur = $db->getResult();
        echo json_encode(array('data_karyawan' => $data_karyawan, 'krj_normal' => $krj_normal, 'lembur' => $lembur));
        //echo json_encode($data_karyawan);
        $db->disconnect();
        break;

// pembayaran gaji -----------------

    case "kode_gaji":
        $db->select('pembayaran', 'max(kd_bayar) as kd_bayar');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "bayar_gaji":
        $nik = $postData['nik'];
        $kd_bayar = $postData['kd_bayar'];
        $p_awal = $postData['p_awal'];
        $p_akhir = $postData['p_akhir'];
        $db->select('absensi', '*', "jenis=0 and nik='".$nik."'");
        $jml_hadir = $db->getResult()['jml'];
        $db->disconnect();
        $db = new DB_Functions();
        $db->connect();
        $db->select('absensi', '*', "jenis=1 and nik='".$nik."'");
        $jml_lembur = $db->getResult()['jml'];
        //echo 'jml_hadir'.$jml_hadir;
        $db = new DB_Functions();
        $db->connect();
        $db->select('pembayaran', '*', "nik='".$nik."' and priode_awal='".$p_awal."' and priode_akhir='".$p_akhir."'");
        $cek_pembayaran = $db->getResult();
        if(count($cek_pembayaran) > 0) {
            echo json_encode(array('pesan' => 'Proses pembayaran karyawan ini untuk periode ini telah di lakukan'));
        } else {
            $hasil = $db->insert('pembayaran', array($kd_bayar, $nik, $p_awal, $p_akhir, $jml_hadir, $jml_lembur), "kd_bayar, nik, priode_awal, priode_akhir, tot_hari_masuk, tot_jam_lembur");
            $bayar_detil = bayar_detail($nik, $kd_bayar);
            if($hasil) {
                if($bayar_detil) {
                    echo json_encode(array('pesan' => 'berhasil'));
                } else {
                    echo json_encode(array('pesan' => 'gagal detail pembayaran'));
                }

            } else {
                echo json_encode(array('pesan' => 'Gagal'));
            }
        }
        $db->disconnect();
        break;

    case "laporan_gaji":
        $p_awal = $postData['p_awal'];
        $p_akhir = $postData['p_akhir'];

        $db->select('pembayaran p inner join karyawan k on p.nik = k.nik', 'p.kd_bayar, p.nik, p.priode_awal, p.priode_akhir, p.tot_hari_masuk, p.tot_jam_lembur, p.take_home_pay, k.nm_karyawan, k.alamat', "priode_akhir between '".$p_awal."' and '".$p_akhir."'");
        //echo json_encode($db->getResult());

        $hasil = array();
        foreach($db->getResult() as $val) {
            $kd_bayar = $val['kd_bayar'];
            $nik = $val['nik'];
            $nama = $val['nm_karyawan'];
            $alamat = $val['alamat'];
            $hari_masuk = $val['tot_hari_masuk'];
            $jam_lembur = $val['tot_jam_lembur'];
            $priode_awal = $val['priode_awal'];
            $priode_akhir = $val['priode_akhir'];
            $take_home_pay = $val['take_home_pay'];
            $data_umum = array('kd_bayar' => $kd_bayar, 'nik'=> $nik, 'nama'=>$nama, 'alamat'=>$alamat, 'tot_hari_masuk'=>$hari_masuk, 'lembur'=>$jam_lembur, 'p_awal'=>$priode_awal, 'p_akhir'=>$priode_akhir, 'take_home_pay'=>$take_home_pay);
            $dbKomponen = new DB_Functions();
            $dbKomponen->connect();
            $dbKomponen->select('pembayaran p inner join pembayaran_detil pd on p.kd_bayar = pd.kd_bayar inner join komponen kom on pd.kd_komp = kom.kd_komp', 'kom.nm_komp, kom.ket, pd.nilai', "p.kd_bayar='".$kd_bayar."'");
            $komponen = $dbKomponen->getResult();
            array_push($hasil, array('data_umum'=>$data_umum, 'komponen'=>$komponen));
            $dbKomponen->disconnect();
        }
        echo json_encode($hasil);
        $db->disconnect();
        break;

    case "login":
       $username = $postData['username'];
       $username = mysql_real_escape_string($username);
       $password = $postData['password'];
       $password = mysql_real_escape_string($password);

       $db->select('user', '*', "username='".$username."' and password='".md5($password)."'");
       $hasil = $db->getResult();

       if(is_array($hasil) && count($hasil) > 0) {
          $_SESSION['isLogin'] = 1;
          $_SESSION['user'] = $hasil['username'];
          echo json_encode(array('pesan'=> 'berhasil', 'hasil' => $hasil));
       } else {
         echo json_encode(array('pesan'=>'gagal', 'hasil' => $hasil));
       }
       $db->disconnect();
       break;

    case "logout":
        $_SESSION = array();
        echo json_encode(array('pesan' => 'Logout berhasil'));
        break;

    case "current_user":
        $username = $_SESSION['user'];
        $db->select('user', '*', "username='".$username."'");
        echo(json_encode($db->getResult()));
        $db->disconnect();
        break;

    case "daftar_user":
        $db->select('user');
        echo json_encode($db->getResult());
        $db->disconnect();
        break;

    case "tambah_user":
        $username = $postData['username'];
        $password = $postData['password'];
        $db->select('user', '*', "username='".$username."'");
        if(count($db->getResult()) > 0) {
            echo json_encode(array("pesan"=>"Username ini sudah ada"));

        } else {
            $insert = $db->insert('user', array($username, md5($password)), 'username, password');
            if($insert) {
                echo json_encode(array("pesan" => "user berhasil ditambahkan"));
            }
        }
        $db->disconnect();
        break;

    case "update_user":
        $username = $_SESSION['user'];
        $password = $postData['password'];
        $ganti = $db->update('user', array("password" => md5($password)), array('username', $username));
        if($ganti) {
            echo json_encode(array('pesan'=>'Berhasil'));
        } else {
            echo json_encode(array('pesan'=>'Gagal'));
        }
        $db->disconnect();
        break;


    case "hapus_user":
        $username = $postData['username'];
        $hasil = $db->delete('user', "username='".$username."'");
        if($hasil) {
            echo json_encode(array("pesan"=>'berhasil di hapus'));
        }
        $db->disconnect();
        break;

}


function bayar_detail($nik, $kd_bayar) {
    $db = new DB_Functions();
    $db->connect();
    $db->select('absensi', 'jml', "nik='".$nik."' and jenis=0");
    $absen_normal = $db->getResult();
    $db->disconnect();
    $hari_masuk = $absen_normal['jml'];
    $db = new DB_Functions();
    $db->connect();
    $db->select('absensi', 'jml', "nik='".$nik."' and jenis=1");
    $jam_lembur = $db->getResult()['jml'];
    $db->disconnect();
    $db = new DB_Functions();
    $db->connect();
    $db->select('karyawan k inner join jabatan j on k.kd_jbt = j.kd_jbt', 'bobot_jbt', "k.nik='".$nik."'");
    $bobot_jbt = $db->getResult()['bobot_jbt'];
    $db->disconnect();

    // hitung gaji pokok
    //echo 'bobot jbt:'.$bobot_jbt;
    $db = new DB_Functions();
    $db->connect();
    $db->select('komponen', 'kd_komp, nilai', "status='0'");
    $nilai_pokok = $db->getResult()['nilai'];
    $kode_gapok = $db->getResult()['kd_komp'];

    //echo 'nilai pokok: '.$nilai_pokok;

    $gaji_pokok = str_replace('BOBOT_JABATAN', $bobot_jbt, $nilai_pokok);
    $gaji_pokok = hitung_string($gaji_pokok);
    echo 'gaji pokok: '.$gaji_pokok.'<br />';
    $db->insert('pembayaran_detil', array($kd_bayar, $kode_gapok, $gaji_pokok), 'kd_bayar, kd_komp, nilai');

    $db->disconnect();

    $db = new DB_Functions();
    $db->connect();
    $db->select('karyawan', 'period_diff(date_format(now(), "%Y%m"), date_format(msk_krj, "%Y%m")) as masa_kerja', "nik='".$nik."'");
    $masa_kerja = $db->getResult()['masa_kerja'];
    $db->disconnect();

    // hitung tunjangan
    $db = new DB_Functions();
    $db->connect();
    $db->select('komponen', 'kd_komp, nilai', "status <> '0'");
    $hasil = $db->getResult();
    if(count($hasil) > 0) {
        foreach($hasil as $val) {
            $nilai = $val['nilai'];
            //echo 'nilai lain: '.$nilai;
            $kd_komp = $val['kd_komp'];
            $nilai = str_replace('HARI_MASUK', $hari_masuk, $nilai);
            $nilai = str_replace('GP', $gaji_pokok, $nilai);
            $nilai = str_replace('MASA_KERJA', $masa_kerja, $nilai);
            $nilai = str_replace(',', '.', $nilai);
            //echo 'replace nilai: '.$nilai;
            $nilai = hitung_string($nilai);
            $isi = new DB_Functions();
            $isi->connect();
            $isi->insert('pembayaran_detil', array($kd_bayar, $kd_komp, $nilai), 'kd_bayar, kd_komp, nilai');
            if($isi) {
                $cb = true;
            } else {
                return false;
            }
            $isi->disconnect();

        }

        $update_tot = new DB_Functions();
        $update_tot->connect();
        $update_tot->select('pembayaran_detil pd inner join pembayaran p on pd.kd_bayar = p.kd_bayar inner join komponen kom on pd.kd_komp = kom.kd_komp', 'sum(pd.nilai) as total', "kom.status <> '3' and p.kd_bayar = '".$kd_bayar."'");
        $update_tot->update('pembayaran', array('take_home_pay'=>$update_tot->getResult()['total']), array('kd_bayar', $kd_bayar));
        $update_tot->disconnect();
    }

    return  $cb;
}


function hitung_string( $mathString )    {
    $mathString = trim($mathString);     // trim white spaces
    // buang yg bukan nomor sama bukan formula matematika
    $mathString = ereg_replace ('[^0-9\+-\*\/\(\) ]', '', $mathString);

    $compute = create_function("", "return (" . $mathString . ");" );
    return 0 + $compute();
}

?>
