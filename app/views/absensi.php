<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Form Kehadiran</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-8">
            <div id="pesan">
            </div>
            <table class="table ">

                <tr>
                    <td>Nik</td>
                    <td>
                      <select ng-model="karyawan" ng-options="k.nik for k in report_karyawan" class="form-control  input-sm"></select>
                    </td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td>{{ karyawan.nm_karyawan }}</td>
                </tr>
                <tr>
                    <td>Jumlah kehadiran</td>
                    <td>
                        <span class="col-sm-4">
                            <input type="text" ng-model="jmlHadir" class="form-control input-sm">
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>Jumlah Jam Lembur</td>
                    <td>
                        <span class="col-sm-4">
                            <input type="text" ng-model="jmlJam" class="form-control input-sm">
                        </span>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <button class="btn btn-danger btn-sm" ng-click="batal()">Batal</button>
                        <button class="btn btn-primary btn-sm" ng-click="simpan()">Simpan
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

