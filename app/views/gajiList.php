<div ng-controller="komponenGajiCtrl" id="komponenGajiWrap">
    <div class="well well-sm">
        <div class="row">
            <div class="col-xs-8 pull-left">
                <h5>Setup Komponen Gaji</h5>
            </div>
            <div class="col-xs-4 pull-right">
                <input ng-model="filter" class="form-control input-sm" placeholder="filter data komponen gaji &hellip;">
            </div>
        </div>
    </div> <!-- well filter -->
    <!-- loading -->

    <!-- eof loading -->

    <div ng-bind-html="status_simpan()"></div>
    <table class="table table-striped table-bordered table-hover" id="tabel_lembur">
        <tr class="success">
            <th>Kode Komponen</th>
            <th>Komponen</th>
            <th>Keterangan</th>
            <th>Nilai</th>
            <th>Status</th>
            <th>&nbsp;</th>
        </tr>
        <tr ng-repeat="komponen in komponen_gaji | filter:filter | orderBy: order_by">
            <td>{{ komponen.kd_komp }}</td>
            <td>{{ komponen.nm_komp }}</td>
            <td>{{ komponen.ket }}</td>
            <td>{{ komponen.nilai }}</td>
            <td>{{ status_komp[komponen.status] }}</td>
            <td><button class="btn btn-danger btn-xs" ng-click="hapus()"><span class="glyphicon glyphicon-trash"></span> Hapus</button></td>
        </tr>
        <tr>
            <td colspan="6">
                <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#tambahKomponen">
                    <span class="glyphicon glyphicon-plus"> Tambah</span></a>
            </td>
        </tr>
    </table>
    <div ng-show="isLoading">
        <p><img src="../public/images/loading.gif" /> loading ...</p>
    </div>
    <!-- modal tambah -->
    <div class="modal fade" id="tambahKomponen">

        <div ng-controller="tambahKomponenGajiCtrl">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Jabatan</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form class="form-inline" role="form" method="post"  enctype="multipart/form-data">
                        <table class="col-md-10">
                            <tr>
                                <td>Kode Komponen</td><td>:</td><td><input type="text" size="5" name="kd_komp" class="form-control" readonly="" ng-model="kode"/>
                                </td>
                            </tr>
                            <tr>
                                <td>Komponen</td><td>:</td><td><input type="text" name="komponen" class="form-control" ng-model="komponen"/></td>
                            </tr>
                            <tr>
                                <td>Keterangan</td><td>:</td><td><input type="text" name="keterangan" class="form-control" ng-model="ket"/></td>
                            </tr>
                            <tr>
                                <td>Nilai</td>
                                <td>:</td>
                                <td><input type="text"  class="form-control" ng-model="nilai" id="nilai"/>
                                </td>
                            </tr>
                            <tr>
                                <td></td><td></td>
                                <td><button class="btn btn-primary btn-xs" data-toggle="modal" href="#formula" >Edit Formula</button>
                                </td>
                            </tr>
                            <tr>
                                <td>Status</td><td>:</td>
                                <td><select class="form-control" ng-model="status_komp">
                                        <option value="0">Gaji Pokok</option>
                                        <option value="1">Tunjangan Tetap</option>
                                        <option value="2">Tunjangan Tidak Tetap</option>
                                        <option value="3">Potongan</option>
                                    </select></td>
                            </tr>
                        </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" id="btnSimpan" ng-click="simpan()">Simpan</button><br /><br />
                </form> <!-- eof form -->
            </div>

        </div> <!-- ng-controller buat simpan -->

    </div>

 <!-- edit formula -->
    <div id="formula" class="modal fade">

        <div class="modal-header">
            <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Formula</h4>
        </div>
        <div class="modal-body">

            <div class="form-horizontal">
                <input type="text" id="txtformula" class="form-control" />
                <p></p>
                <table width="100%">
                    <tr>
                        <td width="30%">Variabel</td>
                        <td width="40%">
                            <select id="variabel" class="form-control">
                                <option value="HARI_MASUK">HARI_MASUK</option>
                                <option value="BOBOT_JABATAN">BOBOT_JABATAN</option>
                                <option value="GP">GP</option>
                                 <option value="MASA_KERJA">MASA_KERJA</option>
                            </select>
                        </td>
                        <td width="30%">
                            <button class="btn btn-primary btn-xs" id="btnTmbhFormula">Tambah</button>
                        </td>
                    </tr>


                </table>
            </div><!-- eof form-horizontal -->

        </div><!-- eof modal body -->
        <div class="modal-footer">
            <button class="btn btn-success btn-xs">Cek Formula</button>
            <button class="btn btn-primary btn-xs " id="btnOk" data-dismiss="modal">OK</button>
        </div>

    </div>
<script>
    $('#btnTmbhFormula').click(function() {
        $formula = $('#txtformula').val();
        $formula_baru = $formula + $('#variabel').val();
        $('#txtformula').val($formula_baru);
        console.log($formula_baru);
    });
    $('#btnOk').click(function() {
        $isiFormula = $('#txtformula').val();
        $('#nilai').val($isiFormula);
    })
</script>
