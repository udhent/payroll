<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Absen</title>
    <link rel="stylesheet" href="public/css/bootstrap.min.css">
    <link rel="stylesheet" href="public/css/bootstrap-modal.css">
    <link rel="stylesheet" href="public/css/digital_clock.css">
    <style>
        body {
            padding-top: 60px;
        }
        img {
            width: 171px;
            height: 180px;
        }
    </style>
</head>
<body ng-app="payrollApp">
<div class="conainer" ng-controller="absenCtrl" id="absen">
    <div class="container">
        <div class="panel panel-success">
            <div class="panel-heading">
                <a href="#" class="panel-title">Absen</a>
            </div>
            <div class="panel-body">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-md-4">

                        </div>
                        <div class="col-xs-6 col-md-4">
                            <div id="reader" style="width:300px;height:250px" class="thumbnail"></div>
                        </div>
                        <div class="col-xs-6 col-md-4">
                            <div class="thumbnail">
                                <img data-src="" id="foto"></img>
                                <div class="caption">
                                    <table class="table" id="data_karyawan">
                                    </table>
                                </div>
                            </div>
                            <div id="coba"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                panel footer
            </div>
        </div>
    </div>
</div>

<script src="lib/js/jquery/jquery-1.10.2.min.js"></script>
<script src="public/js/bootstrap.min.js"></script>
<script src="lib/js/angular/angular.min.js"></script>
<script src="lib/js/angular/angular-file-upload.min.js"></script>
<script src="lib/js/angular/promise-tracker.js"></script>
<script src="lib/js/angular/angular-route.min.js"></script>

<script src="app/js/main.js"></script>
<script src="lib/js/qrcode/qrcode_reader.js"></script>
<script>


    $(document).ready(function() {


        $('#reader').html5_qrcode(function(data){
                // do something when code is read
                data = data.split(" ");
                nik = data[0];
                nama = data[1];
                action = 'absen';
                $postData = JSON.stringify({'action': action, 'nik': nik, 'nama': nama});

                console.log(data);

                $.ajax("app/php/proses.php",
                    {
                        data: $postData,
                        contentType: 'application/json',
                        type: 'POST',
                        success: function(data) {
                            data_karyawan = $.parseJSON(data);
                            nik = data_karyawan.nik;
                            nama = data_karyawan.nm_karyawan;
                            alamat = data_karyawan.alamat;
                            telp = data_karyawan.telp
                            foto = data_karyawan.foto;
                            console.log(foto);
                            if(foto == null) {
                                foto = 'default.png';
                            } else {
                                foto = foto;
                            }

                            $html = '<tr><td>NIK</td><td>:</td><td>' + nik + '</td></tr>' +
                                '<tr><td>Nama</td><td>:</td><td>' + nama + '</td></tr>' +
                                '<tr><td>Telp.</td><td>:</td><td>' + telp + '</td></tr>' +
                                '<tr><td>Alamat</td><td>:</td><td>' + alamat + '</td></tr>';
                            $('#data_karyawan').html($html);

                            $('img').attr('src', 'app/foto/'+foto);
                        }
                    });

            },
            function(error){
                //show read errors
                //console.log(error);
            }, function(videoError){
                //the video stream could be opened
            }
        );
    });

    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-46115475-1', 'bitbucket.org');
    ga('send', 'pageview');

</script>
</body>
</html>
