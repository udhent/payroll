<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#periode">Pilih Periode
            </button> &nbsp;
            <a href="#" id="print">
                <span class="label label-warning">Print
                    <span class="glyphicon glyphicon-print"></span>&nbsp;
                </span>
            </a>&nbsp;
            <a href="pdf/laporan_penggajian.php?p_awal={{ data[0].data_umum.p_awal }}" id="link_pdf">
                <span class="label label-success">Export Slip Gaji Ke PDF
                     <span class="glyphicon glyphicon-file"></span>
                </span>
            </a> &nbsp;
        </h3>
    </div>
    <div class="panel-body" id="printSection">
        <div class="container">
            <div class="row">
                <h2 class="center-content" style="text-align: center">Laporan Pembayaran Gaji</h2>
                <h5 class="center-content" style="text-align: center">Periode</h5>
                <br />
            </div>
            <div class="row span-10">

                    <div class="row" ng-repeat="slip in laporan_gaji">


                            <div class="container">
                                <div class="row">
                                    <table>
                                        <tr>
                                            <hr />
                                        </tr>
                                        <tr>
                                            <td>KD. Slip:</td>
                                            <td>{{ slip.data_umum.kd_bayar }}</td>
                                            <td>Periode</td>
                                            <td>{{ slip.data_umum.p_awal }} - {{ slip.data_umum.p_akhir }}</td>
                                        </tr>
                                        <tr>
                                            <td>NIK:</td>
                                            <td>{{ slip.data_umum.nik }}</td>
                                            <td>Nama:</td>
                                            <td>{{ slip.data_umum.nama }}</td>
                                        </tr>
                                        <tr>
                                            <td>Alamat:</td>
                                            <td>{{ slip.data_umum.alamat }}</td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <div class="center-block">
                                                <br />
                                                <br />
                                            <table class="table-bordered" width="90%">
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Komponen Gaji</th>
                                                        <th>Nilai</th>
                                                    </tr>
                                                    <tr ng-repeat="kom in slip.komponen">
                                                        <td>{{ $index + 1 }}</td>
                                                        <td>{{ kom.nm_komp }}</td>
                                                        <td><div class="pull-right">{{ kom.nilai }}</div></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <table>
                                                <tr>
                                                    <td>Total Lembur</td>
                                                    <td>:</td>
                                                    <td>{{ slip.data_umum.lembur }}</td>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>Jumlah Kehadiran</td>
                                                    <td>:</td>
                                                    <td>{{ slip.data_umum.tot_hari_masuk }}</td>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>Gaji Bersih</td>
                                                    <td>:</td>
                                                    <td>{{ slip.data_umum.take_home_pay }}</td>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                    </div>

            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="periode">
    <div class="modal-header">
        <button class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pilih Periode Laporan</h4>
    </div>
    <div class="modal-body">
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label for="p_awal" class="col-sm-4" control-label>Periode Awal</label>
                <div class="col-sm-8">
                    <div class="input-group date" id="dp2" data-date-format="yyyy-mm-dd">
                            <input class="p_awal form-control" type="text" name="tgl_masuk" id="datepicker" ng-model="p_awal">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>

            </div>
            <div class="form-group">
                <label for="p_akhir" class="col-sm-4" control-label>Periode Akhir</label>
                <div class="col-sm-8">
                    <div class="input-group date" id="dp3" data-date-format="yyyy-mm-dd">
                            <input class="p_akhir form-control" type="text" name="tgl_masuk" id="datepicker" ng-model="p_akhir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>

            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary btn-xs" ng-click="tampilkan()">Tampilkan</button>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#print').click(function(e) {
            e.preventDefault();
            w = window.open();
            //w.document.title('Daftar Karyawan');
            var head = w.document.getElementsByTagName("head")[0];
            if(head) {
                var styleEl = w.document.createElement("style");
                styleEl.type = "text/css";
                head.appendChild(styleEl);
            }
            w.document.write('<link rel="stylesheet" href="../public/css/bootstrap.min.print.css"></link>');
            w.document.write('<style> body {font-size: x-small; } </style>');
            w.document.write($('#printSection').html());
            //w.print();
            //w.close();
        });
        $('#buatPDF').click(function(e) {
            e.preventDefault();
            var pdf = new jsPDF('p', 'in', 'letter')
            , source = $('#printSection')[0]
            , specialElementHandlers = {
                '#bypassme': function(element, renderer) {
                    return true;
                }
            }

            pdf.fromHTML(
                source
                , 0.5
                , 0.5
                , {
                    'width': 7.5
                    , 'elementHandlers': specialElementHandlers
                });
            pdf.output('dataurl')
        });

        $('#dp3').datepicker({
            calendarWeeks: true,
            startDate: '-1m',
            autoclose: true
        });
        $('#dp2').datepicker({
            calendarWeeks: true,
            startDate: '-2m',
            autoclose: true
        });
    });
</script>
