<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <a href="#" id="print">
                <span class="label label-warning">Print
                    <span class="glyphicon glyphicon-print"></span>&nbsp;
                </span>
            </a>&nbsp;
            <a href="pdf/laporan_daftar_karyawan.php" id="buatPDF">
                <span class="label label-success">PDF
                     <span class="glyphicon glyphicon-file"></span>
                </span>
            </a>
        </h3>
    </div>
    <div class="panel-body" id="printSection">
        <div class="container">
            <div class="row">
                <h2 class="center-content" style="text-align: center">Daftar Karyawan</h2>
                <br />
            </div>
            <div class="row">
                <table class="table table-bordered">
                    <tr>
                        <thead>
                            <th>NIK</th><th>Nama Karyawan</th><th>Jabatan</th><th>Divisi</th><th>Mulai Kerja</th><th>Alamat</th>
                        </thead>
                    </tr>
                    <tr ng-repeat="karyawan in report_karyawan">
                        <td>{{ karyawan.nik }}</td>
                        <td>{{ karyawan.nm_karyawan }}</td>
                        <td>{{ karyawan.nm_jbt }}</td>
                        <td>{{ karyawan.nm_divisi }}</td>
                        <td>{{ karyawan.msk_krj }}</td>
                        <td>{{ karyawan.alamat }}</td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#print').click(function(e) {
            e.preventDefault();
            w = window.open();
            //w.document.title('Daftar Karyawan');
            var head = w.document.getElementsByTagName("head")[0];
            if(head) {
                var styleEl = w.document.createElement("style");
                styleEl.type = "text/css";
                head.appendChild(styleEl);
            }
            w.document.write('<link rel="stylesheet" href="../public/css/bootstrap.min.print.css"></link>');
            w.document.write('<style> body {font-size: x-small; } </style>');
            w.document.write($('#printSection').html());
            //w.print();
            //w.close();
        });

    });
</script>
