<div>
    <!-- panel -->
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">Detail Karyawan</h3>
        </div>
        <div class="panel-body">
            <div class="container">
                <div clas="row">
                    <div class="col-sm-2 col-md-1"></div>
                    <div class="col-sm-2 col-md-4">
                        <a href="#/karyawan/{{ detail_karyawan.nik }}" class="thumbnail">
                            <img src="foto/{{ detail_karyawan != '' && detail_karyawan.foto || 'default.png' }}" alt="Foto" width="200" height="200" class="img-thumbnail">
                        </a>
                        <a href="#/karyawan/{{ detail_karyawan.nik }}" class="thumbnail">
                            <span id="qrcode" ng-hide="tempat_code" toggle></span>
                        </a>
                    </div>
                    <div class="col-sm-2 col-md-3"></div>
                    <div class="col-sm-9 col-md-6">
                        <div class="thumbnail">

                            <div class="caption">
                                <h3>{{ detail_karyawan.nm_karyawan }}</h3>
                            </div>
                            <p>
                            <table class="table">
                                <tr>
                                    <td>NIK</td><td>:</td><td id="nik">{{ detail_karyawan.nik }}</td>
                                </tr>
                                <tr>
                                    <td>Nama</td><td>:</td><td id="nama">{{ detail_karyawan.nm_karyawan }}</td>
                                </tr>
                                <tr>
                                    <td>Alamat</td><td>:</td><td>{{ detail_karyawan.alamat }}</td>
                                </tr>
                                <tr>
                                    <td>Telepon</td><td>:</td><td>{{ detail_karyawan.telp }}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Masuk</td><td>:</td><td>{{ detail_karyawan.msk_krj }}</td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <button class="btn btn-primary btn-xs" ng-click="toggle(detail_karyawan.nik, detail_karyawan.nm_karyawan);">
                                            <span class="glyphicon glyphicon-qrcode">
                                            </span> QRcode
                                        </button> &nbsp;
                                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#editKaryawan">
                                            <span class="glyphicon glyphicon-edit"></span> Edit
                                        </button>&nbsp;
                                        <button class="btn btn-danger btn-xs" ng-click="hapus(detail_karyawan.nik)">
                                            <span class="glyphicon glyphicon-trash"></span> Hapus
                                        </button>
                                    </td>
                                </tr>
                            </table>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- eof panel info -->
</div>

<div class="modal fade" id="editKaryawan">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Tambah Karyawan</h4>
    </div>
    <div class="modal-body">

        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label for="inputNama" class="col-sm-2 control-label">Nama</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputNama" name="nama" value="{{ detail_karyawan.nm_karyawan }}" ng-model="detail_karyawan.nm_karyawan">
                </div>
            </div>
            <div class="form-group">
                <label for="inputAlamat" class="col-sm-2 control-label">Alamat</label>
                <div class="col-sm-10">
                    <textarea class="form-control" id="inputNama" name="alamat"ng-model="detail_karyawan.alamat">
                        {{ detail_karyawan.alamat }}
                    </textarea>
                </div>
            </div>

            <div class="form-group">
                <label for="jabatan" class="col-sm-2 control-label">
                    <span>Jabatan</span>
                </label>
                <div class="col-sm-10">
                    <select name="" id="jabatan" ng-model="jabatanOpt" class="form-control">
                        <option ng-repeat="isi_jabatan in list_jabatan.jabatan" value="{{ isi_jabatan.kd_jbt }}">{{ isi_jabatan.nm_jbt }}</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label for="inputTelp" class="col-sm-2 control-label">Telp</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputTelp" name="telp" ng-model="detail_karyawan.telp" value="{{ detail_karyawan.telp }}">
                </div>
            </div>

            <div class="form-group">
                <label for="tglMasuk" class="col-sm-2 control-label">Tanggal Masuk</label>
                <div class="col-sm-10">
                     <div class="input-group date" id="dp4" data-date-format="yyyy-mm-dd">
                        <input class="form-control" type="text" value="{{ detail_karyawan.msk_krj }}" name="tgl_masuk" id="datepicker" ng-model="detail_karyawan.msk_krj">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                    </div>
                </div>
            </div>

        </form>

    </div>

    <div class="modal-footer">
        <button class="btn btn-danger btn-xs" data-dismiss="modal">
            <span class="glyphicon glyphicon-cancel"></span>
            Batal
        <button class="btn btn-primary btn-xs" ng-click="update()">
            <span class="glyphicon glyphicon-save"></span>
            Simpan
        </button>
    </div>
</div>
<script>
        $('#dp4').datepicker({
            calendarWeeks: true,
            startDate: '-3d'
        });
    </script>
