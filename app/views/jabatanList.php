<div ng-controller="jabatanListCtrl" id="jabatanWrap">
    <div class="well well-sm">
        <div class="row">
            <div class="col-xs-8 pull-left">
                <h5>Setup Jabatan</h5>
            </div>
            <div class="col-xs-4 pull-right">
                <input ng-model="filter" class="form-control input-sm" placeholder="filter data jabatan&hellip;">
            </div>
        </div>
    </div> <!-- well filter -->
    <!-- loading -->

    <!-- eof loading -->

    <div ng-bind-html="status_simpan()"></div>
    <table class="table table-striped table-bordered table-hover table-responsive" id="tabel_lembur">
        <tr class="success">
            <th>Kode Jabatan</th>
            <th>Nama Jabatan</th>
            <th>Golongan</th>
            <th>Bobot Jabatan</th>
            <th>&nbsp;</th>
        </tr>
        <tr ng-repeat="jabatan in daftar_jabatan | filter:filter | orderBy: order_by">
            <td>{{ jabatan.kd_jbt }}</td>
            <td>{{ jabatan.nm_jbt }}</td>
            <td>{{ jabatan.golongan }}</td>
            <td>{{ jabatan.bobot_jbt }}</td>
            <td>
              <div class="btn-group">
                <button type="button" class="btn btn-warning btn-xs dropdown-toggle" data-toggle="dropdown">
                <span class="fa fa-exclamation-circle"></span>
                Aksi <span class="caret"></span>
                 </button>
                  <ul class="dropdown-menu" role="menu">
                    <li>
                      <a href="">
                      <span class="fa fa-edit"></span> edit
                      </a>
                    </li>
                    <li>
                      <span class="divider"></span>
                    </li>
                    <li>
                      <a href="" ng-click="hapus(jabatan.kd_jbt)">
                      <span class="fa fa-trash-o"></span> Hapus
                      </a>
                    </li>
                  </ul>
              </div>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#tambahJabatan">
                    <span class="glyphicon glyphicon-plus">Tambah</span></a>
            </td>
        </tr>
    </table>

    <div ng-show="isLoading">
        <p><img src="../public/images/loading.gif" /> loading ...</p>
    </div>
    <!-- modal jabatan -->
    <div class="modal fade" id="tambahJabatan">

        <div ng-controller="tmbhJabatanCtrl">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Tambah Jabatan</h4>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form class="form-inline" role="form" method="post"  enctype="multipart/form-data">
                        <table class="col-md-10">
                            <tr>
                                <td>Kode Jabatan</td><td>:</td><td><input type="text" size="5" name="nik" class="form-control" readonly="" value="{{ kode_baru }}" /></td>
                            </tr>
                            <tr>
                                <td>Jabatan</td><td>:</td><td><input type="text" name="jabatan" class="form-control" ng-model="jabatan"/></td>
                            </tr>
                            <tr>
                                <td>Golongan</td><td>:</td>
                                <td>
                                    <select name="gol" class="form-control" ng-model="kd_gol">

                                        <option ng-repeat="gol in daftar_gol" value="{{ gol.kd_gol }}">{{ gol.nm_gol }}</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td>Bobot</td><td>:</td><td><input type="text" name="bobot" class="form-control" ng-model="bobot"/></td>
                            </tr>
                        </table>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" id="btnSimpan" ng-click="simpan()">Simpan</button><br /><br />
                </form> <!-- eof form -->
            </div>

        </div> <!-- ng-controller buat simpan -->

    </div>
