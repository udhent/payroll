<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">User</h3>
    </div>
    <div class="panel-body">
        <table class="table table-stripped tabel-hover table-bordered">
            <tr>
                <th>#</th>
                <th>Username</th>
                <th></th>
            </tr>
            <tr ng-repeat="user in users">
                <td>{{ $index + 1 }}</td>
                <td>
                    {{ user.username }}

                </td>
                <td>
                    <button class="btn btn-danger btn-xs" ng-click="hapus()">Hapus</button>
                </td>
            </tr>
        </table>
        <button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#modalTambah">Tambah</button>
    </div>
</div>


<div class="modal fade" id="modalTambah">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Tambah User</h3>
    </div>
    <div class="modal-body">
        <table class="table table-stripped">
            <tr>
                <td class="col-md-3">Username</td>
                <td  class="col-md-3"><input type="text" class="form-control" ng-model="username"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td class="col-md-3"><input type="password" class="form-control" ng-model="pass1"></td>
            </tr>
            <tr>
                <td>Konfirmasi Password</td>
                <td><input type="password" class="form-control" ng-model="pass2"></td>
            </tr>

            <tr>
                <td colspan="2"><button class="btn btn-primary btn-xs" ng-click="tambah()">Simpan</button></td>
            </tr>
        </table>
    </div>
</div>
