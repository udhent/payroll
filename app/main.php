
<!-- navbar -->
<div class="nav navbar-inverse navbar-fixed-top" role="navigation" ng-controller="loginCtrl">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle colapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="#" class="navbar-brand"><span class="fa fa-home"></span> Payroll System</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdwon-toggle" data-toggle="dropdown">Signed in as <?php if(isset($_SESSION['user'])) { echo $_SESSION['user']; } else { echo "anonymous"; } ?>&nbsp;<span class="fa fa-chevron-down"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#/profile">Profile <span class="glyphicon glyphicon-cog pull-right"></span></a>
                        </li>
                        <li>
                            <a href="#/user">User <span class="fa fa-user pull-right"></span>
                        </li>
                        <li><a ng-click="logout()"><span class="glyphicon glyphicon-off pull-right"></span> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end of navbar -->

<div class="container">
    <div class="row">

        <!-- sidebar -->

        <div class="col-md-3" id="panel_menu">
            <!-- panel data -->
          <div class="panel-group" id="accordion">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#menu_data" id="link_menu_data">Data <span class="glyphicon glyphicon-folder-open pull-right"></span></a></h4>
                </div>

                <div id="menu_data" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked" id="nav_data">
                            <li id="karyawan">
                                <a href="#/karyawan">
                                    <span class="fa fa-users pull-right"></span>
                                    Data Karyawan
                                </a>
                            </li>
                            <li id="absensi">
                                <a href="#/absensi">
                                    Absensi
                                    <span class="fa fa-user pull-right"></span>
                                </a>
                            </li>
                            <li id="bayarGaji">
                                <a href="#/pembayaranGaji">
                                    Pembayaran Gaji
                                    <span class="fa fa-tasks pull-right"></span>
                                </a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>

            <!-- panel laporan -->

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h5 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#menu_laporan" id="laporan_link">Laporan
                            <span class="glyphicon glyphicon-th-list pull-right"></span>
                        </a>
                    </h5>
                </div>
                <div id="menu_laporan" class="panel-collapse collapse">
                <div class="panel-body">

                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#/report/karyawan">Laporan Daftar karyawan<span class="glyphicon glyphicon-tags pull-right"></span></a></li>
                            <li><a href="#/report/gaji">Laporan Pembayaran Gaji<span class="fa fa-money pull-right"></span></a></li>
                        </ul>
                    </div><!-- panel-body -->
                </div><!-- panel-collapse -->
            </div><!-- panel primary -->

            <!-- panel isi -->

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#setting" id="menu_setting">
                            Setting<span class="fa fa-gears pull-right"></span>
                        </a>
                    </h4>
                </div>

                <div id="setting" class="panel-collapse collapse">
                    <div class="panel-body">
                        <ul class="nav nav-pills nav-stacked" id="nav_samping">

                            <li id="jabatan">
                                <a href="#/jabatan">
                                    <span class="glyphicon glyphicon-file pull-right"></span>
                                    Setup Jabatan
                                </a>
                            </li>

                            <li id="golongan">
                                <a href="#/divisi">
                                    <span class="glyphicon glyphicon-book pull-right"></span>
                                    Setup Divisi
                                </a>
                            </li>

                            <li id="komponenGaji">
                                <a href="#/komponenGaji">
                                    <span class="glyphicon glyphicon-usd pull-right"></span>
                                    Komponen Gaji
                                </a>
                            </li>
                            <li id="lembur">
                                <a href="#/lembur">
                                    <span class="glyphicon glyphicon-usd pull-right"></span>
                                    Lembur
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
          </div>
        </div>

        <!-- end of sidebar -->

        <!-- content -->
        <div class="col-md-9" ng-view class="example-animate-container">
            <h1>Content Goes here</h1>
        </div>
    </div>
</div>

<script src="lib/js/jquery/jquery-1.10.2.min.js"></script>
<script src="public/js/bootstrap.min.js"></script>
<script src="public/js/bootstrap-modalmanager.js"></script>
<script src="public/js/bootstrap-modal.js"></script>
<script src="public/js/typehead.min.js"></script>
<script src="lib/js/jquery/datepick.js"></script>
<script src="lib/js/angular/angular.min.js"></script>
<script src="lib/js/angular/angular-route.min.js"></script>
<script src="lib/js/angular-file-upload.js"></script>
<script src="lib/js/angular/angular-file-upload.min.js"></script>
<script src="lib/js/angular/promise-tracker.js"></script>
<script src="lib/js/qrcode/qrcode_generator.js"></script>
<script src="js/main.js"></script>
<script src="public/js/bootstrap-select.min.js"></script>
<script src="lib/js/jquery.PrintArea.js"></script>
<script src="lib/js/jspdf.js"></script>
<script src="lib/js/jspdf.plugin.standard_fonts_metrics.js"></script>
<script src="lib/js/jspdf.plugin.split_text_to_size.js"></script>
<script src="lib/js/jspdf.plugin.from_html.js"></script>

<script>
    $(document).ready(function() {
        var $nav_samping = $('#nav_samping');
        var $karyawan = $('#karyawan');
        var $golongan = $('golongan');
        var $lembur = $('#lembur');
        var $prestasi = $('#prestasi');
        var $laporan = $('#laporan');
        var $pgbar = $('#pgbar');
        $pgbar.hide();


        var $selectFile = $('#select_file');

        $(document).on('change', '#select_file', function() {
            $pgbar.removeAttr('style');
        });

        $('.nav>li').click(function(e) {
            $('.nav>li.active').removeClass('active');

            $(this).addClass('active');

        });

        $('#btnTmbhLembur').click(function() {alert('hello');});


        $('#link_menu_data').click(function(e) {
            e.preventDefault();
        });

        $('#menu_setting').click(function(e) {
            e.preventDefault();
        });

        $('#laporan_link').click(function(e) {
            e.preventDefault();
        });

       // $('.inputnik .typehead').typehead({
       //     name: 'nik',
       //    prefetch : {'name': 'nasa'},
       //     limit: 10
       // })

    });


</script>
