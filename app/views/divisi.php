<div class="well well-sm">
        <div class="row">
            <div class="col-xs-8 pull-left">
                <h5>Setup Divisi</h5>
            </div>
            <div class="col-xs-4 pull-right">
                <input ng-model="filter" class="form-control input-sm" placeholder="filter data divisi&hellip;">
            </div>
        </div>
    </div> <!-- well filter -->
    <!-- loading -->

<table class="table table-stripped table-bordered table-hover table-responsive">
 <tr class="success">
     <th>ID Divisi</th>
     <th>Nama Divisi</th>
     <th><button class="btn btn-primary btn-xs" data-toggle="modal" data-target="#tambahDivisi">Tambah</button></th>
 </tr>

 <tr ng-repeat="divisi in list_divisi">
    <td>{{ divisi.id }}</td>
    <td>{{ divisi.nm_divisi }}</td>
    <td>
        <button class="btn btn-primary btn-xs" id="btn_edit" ng-click="get_divisi(divisi.id)" data-toggle="modal" data-target="#editDivisi">
            Edit
        </button>
        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#editDivisi" ng-click="hapus(divisi.id)">
            Hapus
        </button>
    </td>
</tr>
</table>

<!-- modal tambah divisi -->
<div class="modal fade" id="tambahDivisi">
    <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title">Tambah Divisi</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label for="nama_divisi">Nama Divisi</label>
                <input type="text" class="form-control input-sm" ng-model="nama_divisi"></input>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary btn-xs" ng-click="simpan()">Simpan</button>
    </div>
</div>

<!-- modal edit -->

<div class="modal fade" id="editDivisi">
    <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title">Edit Divisi</h3>
    </div>
    <div class="modal-body">

        <form class="form-horizontal">
            <div class="form-group">
                <label for="nama_divisi">Nama Divisi</label>
                <input type="text" class="form-control input-sm" ng-model="detail_divisi.nm_divisi" id="nama_divisi"></input>
                <input type="hidden" ng-model="detail_divisi.id"/>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary btn-xs" ng-click="update(detail_divisi.id, detail_divisi.nm_divisi)">Simpan</button>
    </div>
</div>

<!-- sukses hapus -->

<div class="modal fade" id="hapus_modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 class="modal-title">Hapus Divisi</h3>
    </div>
    <div class="modal-body">
        {{pesan}}
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary btn-xs" data-dismis="modal">OK</button>
    </div>
</div>

