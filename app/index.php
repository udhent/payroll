<?php
 session_start();
//$_SESSION['isLogin'] = 1;
//$_SESSION = array();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Payroll System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="public/css/bootstrap.css"></link>
    <link rel="stylesheet" href="public/css/font-awesome.min.css"></link>
    <link rel="stylesheet" href="public/css/docs.css"></link>
    <link rel="stylesheet" href="public/css/bootstrap-modal-bs3patch.css"></link>
    <link rel="stylesheet" href="public/css/boostrap-modal.css"></link>
    <link rel="stylesheet" href="lib/css/datepick.css"></link>

    <style>
        body {
            padding-top: 70px;
        }
        .modal {
            overflow-y: auto;
        }
        .datepicker {
            z-index:1051 !important;
        }

        .dropdown {
            cursor: pointer;
        }

        .center-content {
            text-align: center;
        }
        .panel {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }

        .panel-heading {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }

        .well {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }

        .form-control {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }

        .btn {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }

        .nav ul li.active {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }

        .modal {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }
        .label {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }
        .nav-pills>li>a {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }
        .alert {
            -webkit-border-radius: 0px !important;
            -moz-border-radius: 0px !important;
            border-radius: 0px !important;
        }
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>

    <![endif]-->
</head>
<body ng-app="payrollApp">

<?php

if(empty($_SESSION)) {
    include "login.php";
} else {
    if($_SESSION['isLogin'] = 1) {
        include "main.php";
    } else {
        echo "login lagi";
    }
}

?>
</body>
</html>
