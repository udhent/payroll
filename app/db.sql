-- MySQL dump 10.13  Distrib 5.5.34, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: payroll
-- ------------------------------------------------------
-- Server version	5.5.34-0ubuntu0.13.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `payroll`
--

/*!40000 DROP DATABASE IF EXISTS `payroll`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `payroll` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `payroll`;

--
-- Table structure for table `absensi`
--

DROP TABLE IF EXISTS `absensi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `absensi` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `jenis` int(1) NOT NULL,
  `nik` varchar(3) NOT NULL,
  `jml` int(3) NOT NULL,
  `tgl_absen` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `absensi`
--

LOCK TABLES `absensi` WRITE;
/*!40000 ALTER TABLE `absensi` DISABLE KEYS */;
INSERT INTO `absensi` VALUES (1,0,'001',20,'2014-01-13'),(2,1,'001',5,'2014-01-13'),(3,0,'002',20,'2014-01-13'),(4,1,'002',0,'2014-01-13'),(5,0,'003',20,'2014-01-14'),(6,1,'003',7,'2014-01-14'),(7,0,'004',25,'2014-01-14'),(8,1,'004',7,'2014-01-14');
/*!40000 ALTER TABLE `absensi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `kd_user` varchar(4) NOT NULL,
  `user_name` varchar(12) NOT NULL,
  `user_pass` varchar(60) NOT NULL,
  PRIMARY KEY (`kd_user`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('0001','admin','rahasia');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `divisi`
--

DROP TABLE IF EXISTS `divisi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `divisi` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nm_divisi` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `divisi`
--

LOCK TABLES `divisi` WRITE;
/*!40000 ALTER TABLE `divisi` DISABLE KEYS */;
INSERT INTO `divisi` VALUES (1,'IT'),(2,'Marketing'),(3,'Akunting');
/*!40000 ALTER TABLE `divisi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `golongan`
--

DROP TABLE IF EXISTS `golongan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `golongan` (
  `kd_gol` varchar(1) NOT NULL,
  `nm_gol` varchar(5) NOT NULL,
  PRIMARY KEY (`kd_gol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `golongan`
--

LOCK TABLES `golongan` WRITE;
/*!40000 ALTER TABLE `golongan` DISABLE KEYS */;
INSERT INTO `golongan` VALUES ('1','I'),('2','II'),('3','III'),('4','IV');
/*!40000 ALTER TABLE `golongan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `golongan_sub`
--

DROP TABLE IF EXISTS `golongan_sub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `golongan_sub` (
  `kd_sub` varchar(1) NOT NULL,
  `nm_sub` varchar(1) NOT NULL,
  PRIMARY KEY (`kd_sub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `golongan_sub`
--

LOCK TABLES `golongan_sub` WRITE;
/*!40000 ALTER TABLE `golongan_sub` DISABLE KEYS */;
INSERT INTO `golongan_sub` VALUES ('1','A'),('2','B'),('3','C'),('4','X');
/*!40000 ALTER TABLE `golongan_sub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jabatan`
--

DROP TABLE IF EXISTS `jabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jabatan` (
  `kd_jbt` varchar(2) NOT NULL,
  `nm_jbt` varchar(30) NOT NULL,
  `kd_gol` varchar(1) NOT NULL,
  `bobot_jbt` int(11) NOT NULL,
  PRIMARY KEY (`kd_jbt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jabatan`
--

LOCK TABLES `jabatan` WRITE;
/*!40000 ALTER TABLE `jabatan` DISABLE KEYS */;
INSERT INTO `jabatan` VALUES ('01','Direktur Utama','1',8),('02','Manajer Pemasaran','2',6),('03','Manajer Personalia','2',4000),('04','Staff IT','1',4000);
/*!40000 ALTER TABLE `jabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `karyawan`
--

DROP TABLE IF EXISTS `karyawan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `karyawan` (
  `nik` varchar(3) NOT NULL,
  `nm_karyawan` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `kd_jbt` varchar(2) NOT NULL,
  `kd_divisi` int(2) DEFAULT NULL,
  `msk_krj` date NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`nik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `karyawan`
--

LOCK TABLES `karyawan` WRITE;
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;
INSERT INTO `karyawan` VALUES ('001','Himawan','Jl. Mangga 34 Banguntapan','0274123412','02',1,'2012-04-27',NULL,NULL),('002','Umar','Jl. Embar 55 Yogyakarta','027411223344','03',1,'2000-04-27',NULL,NULL),('003','H. Damijo','Jl. Suratmaja 34, Yogya','0274928472','01',1,'2007-03-01',NULL,NULL),('004','Nasaruddin','Kalibata','021','01',1,'2013-12-04','',NULL);
/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kebijakan`
--

DROP TABLE IF EXISTS `kebijakan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kebijakan` (
  `kd_bijak` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `jml_hari_kerja` int(11) NOT NULL,
  `jam_mulai` time NOT NULL,
  PRIMARY KEY (`kd_bijak`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kebijakan`
--

LOCK TABLES `kebijakan` WRITE;
/*!40000 ALTER TABLE `kebijakan` DISABLE KEYS */;
INSERT INTO `kebijakan` VALUES (01,6,'09:00:00');
/*!40000 ALTER TABLE `kebijakan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kehadiran`
--

DROP TABLE IF EXISTS `kehadiran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kehadiran` (
  `kd_kehadiran` varchar(4) NOT NULL,
  `nik` varchar(3) DEFAULT NULL,
  `jml_hadir` int(2) DEFAULT NULL,
  `periode_awal` date DEFAULT NULL,
  `periode_akhir` date DEFAULT NULL,
  `sts_kerja` int(1) DEFAULT NULL,
  PRIMARY KEY (`kd_kehadiran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kehadiran`
--

LOCK TABLES `kehadiran` WRITE;
/*!40000 ALTER TABLE `kehadiran` DISABLE KEYS */;
/*!40000 ALTER TABLE `kehadiran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `komponen`
--

DROP TABLE IF EXISTS `komponen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `komponen` (
  `kd_komp` varchar(3) NOT NULL,
  `nm_komp` varchar(30) NOT NULL,
  `ket` varchar(50) NOT NULL,
  `nilai` varchar(50) NOT NULL,
  `status` enum('0','1','2','3') NOT NULL DEFAULT '0' COMMENT '0=G Pokok, 1=Tj Tetap, 2=Tj Tidak Tetap, 3=potongan',
  PRIMARY KEY (`kd_komp`),
  UNIQUE KEY `nm_komp` (`nm_komp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `komponen`
--

LOCK TABLES `komponen` WRITE;
/*!40000 ALTER TABLE `komponen` DISABLE KEYS */;
INSERT INTO `komponen` VALUES ('001','Gaji Pokok','-','BOBOT_JABATAN*1000','0'),('002','Tunjangan Makan','-','4000*HARI_MASUK','1'),('003','Tunj Transportasi','-','6000*HARI_MASUK','1'),('004','Asuransi Kesehatan','-','GP*0,02','3'),('005','tes tambh','-','HARI_MASUK*7000','1');
/*!40000 ALTER TABLE `komponen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `komponen_jabatan`
--

DROP TABLE IF EXISTS `komponen_jabatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `komponen_jabatan` (
  `kd_gaji` varchar(4) NOT NULL,
  `kd_jbt` varchar(2) NOT NULL,
  `kd_sub` varchar(1) NOT NULL,
  PRIMARY KEY (`kd_gaji`),
  UNIQUE KEY `jbt_sub` (`kd_jbt`,`kd_sub`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `komponen_jabatan`
--

LOCK TABLES `komponen_jabatan` WRITE;
/*!40000 ALTER TABLE `komponen_jabatan` DISABLE KEYS */;
INSERT INTO `komponen_jabatan` VALUES ('0001','01','1'),('0002','02','1');
/*!40000 ALTER TABLE `komponen_jabatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `komponen_jabatan_detil`
--

DROP TABLE IF EXISTS `komponen_jabatan_detil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `komponen_jabatan_detil` (
  `kd_gaji` varchar(4) NOT NULL,
  `kd_komp` varchar(3) NOT NULL,
  PRIMARY KEY (`kd_gaji`,`kd_komp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `komponen_jabatan_detil`
--

LOCK TABLES `komponen_jabatan_detil` WRITE;
/*!40000 ALTER TABLE `komponen_jabatan_detil` DISABLE KEYS */;
INSERT INTO `komponen_jabatan_detil` VALUES ('0001','001'),('0001','002'),('0001','003'),('0001','004'),('0001','005'),('0001','006'),('0002','001'),('0002','002'),('0002','003'),('0002','004'),('0002','006');
/*!40000 ALTER TABLE `komponen_jabatan_detil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lembur`
--

DROP TABLE IF EXISTS `lembur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lembur` (
  `kd_lembur` int(2) NOT NULL AUTO_INCREMENT,
  `nilai` double NOT NULL,
  PRIMARY KEY (`kd_lembur`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lembur`
--

LOCK TABLES `lembur` WRITE;
/*!40000 ALTER TABLE `lembur` DISABLE KEYS */;
INSERT INTO `lembur` VALUES (1,50000);
/*!40000 ALTER TABLE `lembur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembayaran`
--

DROP TABLE IF EXISTS `pembayaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembayaran` (
  `kd_bayar` varchar(4) NOT NULL,
  `nik` varchar(3) NOT NULL,
  `priode_awal` date NOT NULL,
  `priode_akhir` date NOT NULL,
  `tot_hari_masuk` int(2) DEFAULT NULL,
  `tot_jam_lembur` double NOT NULL,
  `take_home_pay` double NOT NULL,
  PRIMARY KEY (`kd_bayar`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembayaran`
--

LOCK TABLES `pembayaran` WRITE;
/*!40000 ALTER TABLE `pembayaran` DISABLE KEYS */;
INSERT INTO `pembayaran` VALUES ('0001','001','2013-12-25','2014-01-25',20,5,0),('0002','002','2013-12-25','2014-01-25',20,0,0),('0003','003','2013-12-25','2014-01-25',20,7,0),('0004','004','2013-12-25','2014-01-25',25,7,258000),('0005','001','2014-01-25','2014-02-25',20,5,346000);
/*!40000 ALTER TABLE `pembayaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pembayaran_detil`
--

DROP TABLE IF EXISTS `pembayaran_detil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pembayaran_detil` (
  `kd_bayar` varchar(4) NOT NULL,
  `kd_komp` varchar(3) NOT NULL,
  `nilai` double NOT NULL,
  PRIMARY KEY (`kd_bayar`,`kd_komp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pembayaran_detil`
--

LOCK TABLES `pembayaran_detil` WRITE;
/*!40000 ALTER TABLE `pembayaran_detil` DISABLE KEYS */;
INSERT INTO `pembayaran_detil` VALUES ('0001','001',6000),('0001','002',92000),('0001','003',138000),('0001','004',120),('0002','001',4000000),('0002','002',80000),('0002','003',120000),('0002','004',80000),('0003','001',8000),('0003','002',80000),('0003','003',120000),('0003','004',160),('0004','001',8000),('0004','002',100000),('0004','003',150000),('0004','004',160),('0005','001',6000),('0005','002',80000),('0005','003',120000),('0005','004',120),('0005','005',140000);
/*!40000 ALTER TABLE `pembayaran_detil` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `kd_user` int(3) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `level` int(1) NOT NULL,
  PRIMARY KEY (`kd_user`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'nasa','230c5c9d495e3bf392ef2b8098e51921',0),(2,'admin','21232f297a57a5a743894a0e4a801fc3',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-01-16 18:29:52
