<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Form Pembayaran Gaji</h3>
    </div>
    <div class="panel-body">

        <div class="container">
            <div class="row">
                <form class="form-inline" role="form">
                    <div class="col-sm-2">
                        Kode Bayar
                    </div>
                    <div class="col-sm-2">
                        <input ng-model="kd_bayar" class="form-control input-sm" type="text" disabled>
                    </div>
                    <div class="col-sm-1">
                        Periode
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date" id="dp3" data-date-format="yyyy-mm-dd">
                            <input class="p_awal form-control" type="text" name="tgl_masuk" id="datepicker" ng-model="p_awal">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                        </div>

                    </div>
                    <div class="col-sm-1">
                        Sampai
                    </div>
                    <div class="col-sm-3">
                        <div class="input-group date" id="dp2" data-date-format="yyyy-mm-dd">
                            <input class="p_akhir form-control" type="text" name="tgl_masuk" id="datepicker" ng-model="p_akhir">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </span>
                        </div>

                    </div>
                </form>
            </div>
            <!--- panel karyawan -->
            <br />
            <div class="row">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Karyawan</h3>
                    </div>
                    <div class="panel-body">

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="nik" class="col-sm-2">NIK</label>
                                <div class="col-sm-2">
                                    <select ng-model="karyawan" class="form-control input-sm" ng-options="k.nik for k in daftar_karyawan.data_karyawan">
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    {{ karyawan.nm_karyawan }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="nama">Jabatan</label>
                                <div class="col-sm-4">
                                    {{ karyawan.nm_jbt }}
                                </div>
                                <label class="col-sm-2" for="golongan">Divisi</label>
                                <div class="col-sm-4">
                                    {{ karyawan.nm_divisi }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2" for="ttlMasuk">Total Hari Masuk</label>
                                <div class="col-sm-1" id="hari_kerja">
                                    <label ng-repeat="krj in daftar_karyawan.krj_normal | filter:karyawan.nik" ng-model="jml_krj">{{ krj.jml }}</label>
                                </div>
                                <label class="col-sm-2" for="ttljam">Total Lembur</label>
                                <div class="col-sm-1" id="jam_lembur">
                                    <label ng-repeat="jml_lembur in daftar_karyawan.lembur | filter:karyawan.nik" ng-model="jmlh_lembur">{{ jml_lembur.jml }}</label>
                                </div>
                            </div>
                        </form>
                   </div><!-- eof panel body -->
                </div><!-- eof panel -->

            </div>
        </div>


    </div>
    <div class="panel-footer">
        <button class="btn btn-primary btn-xs pull-left" ng-click="bayar()">Bayarkan Gaji</button>&nbsp;
    </div>
</div>
<script>
        $('#dp3').datepicker({
            calendarWeeks: true,
            startDate: '-1m'
        });
        $('#dp2').datepicker({
            calendarWeeks: true,
            startDate: '-3d'
        });




 </script>
