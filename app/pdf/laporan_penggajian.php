<?php
include "../lib/php/DB_Functions.php";
require ('../lib/php/fpdf.php');
date_default_timezone_set('UTC');

if(isset($_GET['p_awal']) and isset($_GET['p_akhir'])) {
    $p_awal = $_GET['p_awal'];
    $p_akhir = $_GET['p_akhir'];
} else {
    $p_awal = '0000-00-00';
    $p_akhir = '0000-00-00';
}


class PDF extends FPDF

//Cell(width, height, txt, border, ln) ln(0 ke kanan, 1 baris baru, 2 bawah)

{
    function data_karyawan($kd_bayar, $periode, $nik, $nama, $alamat)  {

        $this->Cell(190,0, '', 1);
        $this->Ln(5);
        $this->Cell(70);
        $this->Cell(0,0, 'Slip Gaji');
        $this->Ln(10);

        $this->SetFont('Arial','',10);
        $this->Cell(20,0,'Kd. Slip');
        $this->Cell(60,0,$kd_bayar);
        $this->Cell(20,0,'Periode');
        $this->Cell(30,0,$periode);
        $this->Ln(5);

        $this->Cell(20,0,'NIK');
        $this->Cell(60,0,$nik);
        $this->Cell(20,0,'Nama');
        $this->Cell(30,0,$nama);
        $this->Ln(5);

        $this->Cell(20,0,'Alamat');
        $this->Cell(60,0, $alamat);
        $this->Ln(5);


        $w = array(20, 60, 75);
    }


    function tabel($header, $data) {

        //lebar kolom
        $w = array(10, 100, 40);

        //header
        $this->Cell(20);
        for($i=0; $i<count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
        $this->Ln();
        $x = 1;
        foreach($data as $row)
        {
            $this->Cell(20);
            $this->Cell($w[0],6,$x,'LR');
            $this->Cell($w[1],6,$row['nm_komp'],'LR');
            $this->Cell($w[2],6,number_format($row['nilai']),'LR',0,'R');
            $this->Ln();
            $x++;
        }
        $this->Cell(20);
        $this->Cell(array_sum($w),0,'','T');
        $this->Ln(10);

    }

    function footer($lembur=Null, $hari_masuk=Null, $take_home_pay=Null) {

        if($lembur != Null and $hari_masuk != Null and $take_home_pay != Null)
            {
                 $this->Cell(30,0,'Total Lembur');
                $this->Cell(60,0,$lembur);
                $this->Ln(5);
                $this->Cell(30,0,'Kehadiran');
                $this->Cell(30,0,$hari_masuk);
                $this->Ln(5);
                $this->Cell(30,0, 'Take Home Pay');
                $this->Cell(30,0, $take_home_pay);
                $this->Ln(5);
            } else {

            }
     }
}

$pdf = new PDF();
$pdf->SetFont('Arial','',14);
$pdf->AddPage();


$db = new DB_Functions();
$db->connect();


        $db->select('pembayaran p inner join karyawan k on p.nik = k.nik', 'p.kd_bayar, p.nik, p.priode_awal, p.priode_akhir, p.tot_hari_masuk, p.tot_jam_lembur, p.take_home_pay, k.nm_karyawan, k.alamat', "priode_akhir between '".$p_awal."' and '".$p_akhir."'");
        //echo json_encode($db->getResult());

        $hasil = array();
        $jml_cetak = 0;
        foreach($db->getResult() as $val) {


            $kd_bayar = $val['kd_bayar'];
            $nik = $val['nik'];
            $nama = $val['nm_karyawan'];
            $alamat = $val['alamat'];
            $hari_masuk = $val['tot_hari_masuk'];
            $jam_lembur = $val['tot_jam_lembur'];
            $priode_awal = $val['priode_awal'];
            $priode_akhir = $val['priode_akhir'];
            $take_home_pay = $val['take_home_pay'];
            $data_umum = array('kd_bayar' => $kd_bayar, 'nik'=> $nik, 'nama'=>$nama, 'alamat'=>$alamat, 'tot_hari_masuk'=>$hari_masuk, 'lembur'=>$jam_lembur, "p_akhir between '".$priode_awal."' and '".$priode_akhir."'");
            $dbKomponen = new DB_Functions();
            $dbKomponen->connect();
            $dbKomponen->select('pembayaran p inner join pembayaran_detil pd on p.kd_bayar = pd.kd_bayar inner join komponen kom on pd.kd_komp = kom.kd_komp', 'kom.nm_komp, kom.ket, pd.nilai', "p.kd_bayar='".$kd_bayar."'");
            $komponen = $dbKomponen->getResult();

            if($jml_cetak != 0) {
                if(($jml_cetak % 3) == 0) {
                    $pdf->AddPage();
                }

            }
            $pdf->data_karyawan($kd_bayar, $priode_awal." ".$priode_akhir, $nik, $nama, $alamat);
            $header = array('No', 'Komponen Gaji', 'Nilai');
            $pdf->tabel($header,$komponen);
            $pdf->footer($jam_lembur, $hari_masuk, $take_home_pay);
            $dbKomponen->disconnect();
            $jml_cetak++;
        }
        //print_r($komponen);
$db->disconnect();
$pdf->Output();
