/*
* file yang mengelola routing, view yang harus di tampilkan request ke database
* december 2014
* author Nasa
*/
var payrollApp = angular.module('payrollApp',
    ['ngRoute', 'angularFileUpload']);

// --------------------- routing conroller --------------//

payrollApp.config(function($routeProvider, $locationProvider) {
    //$locationProvider.html5Mode(true);
    $routeProvider.when('/', {
        templateUrl: 'views/main_view.php'
    }).when("/karyawan", {
        templateUrl: "views/karyawanList.php",
        controller: "karyawanListCtrl"
    }).when("/karyawan/:nik", {
            templateUrl : "views/karyawanDetail.php",
            controller: "karyawanDetailCtrl"
        }).when('/jabatan', {
            templateUrl: 'views/jabatanList.php',
            controller: 'jabatanListCtrl'
        }).when('/komponenGaji', {
            templateUrl: "views/gajiList.php",
            controller: "komponenGajiCtrl"
        }).when('/report/karyawan', {
            templateUrl: 'reports/daftar_karyawan.php',
            controller: 'reportDftKaryawanCtrl'
        }).when('/absensi', {
            templateUrl: 'views/absensi.php',
            controller: 'absensiCtrl'
        }).when('/pembayaranGaji', {
            templateUrl: 'views/pembayaran_gaji.php',
            controller: 'bayarGajiCtrl'
        }).when('/lembur', {
            templateUrl: 'views/lemburList.php',
            controller: 'lemburCtrl'
        }).when('/divisi', {
            templateUrl: 'views/divisi.php',
            controller: 'divisiCtrl'
        }).when('/profile', {
            templateUrl: 'views/profile.php',
            controller: 'profileCtrl'
        }).when('/user', {
            templateUrl: 'views/user.php',
            controller: 'userCtrl'
        }).when('/report/gaji', {
            templateUrl: 'reports/gaji.php',
            controller: 'rptGajiCtrl'
        }).otherwise({ redirectTo: '/' });
});

// ------------ Factory list karyawan --------------------//

payrollApp.factory('listKaryawan', function($http) {
    return {
        getKaryawan: function() {
            return $http.post('php/proses.php', {'action': 'list_karyawan'})
        }
    }
});

payrollApp.controller('karyawanListCtrl', function($scope, listKaryawan) {

    $scope.isLoading = true;

    listKaryawan.getKaryawan().success(function(data) {
       $scope.daftar_karyawan = data;
       console.log(data);
       $scope.isLoading = false;
    });

});

//---------------- detail karyawan --------------------------------//
payrollApp.controller('karyawanDetailCtrl', ['$scope', '$routeParams', '$http', '$location', function($scope, $routeParams, $http, $location) {
    $postData = {'action': 'detail_karyawan', 'nik': $routeParams.nik};
    $http.post('php/proses.php', $postData).success(function(data) {
        $scope.detail_karyawan = data;
    });
    //$scope.nik = $routeParams.nik;
    var jmlh_qr = 0;

    $scope.toggle = function(nik, nama) {
        $scope.$broadcast('event:toggle');
        if (jmlh_qr == 0) {
            var qrCode = new QRCode('qrcode', {
                text: nik + ' ' + nama,
                width: 220,
                height: 200,

            });
             jmlh_qr = 1;

             } else {
            return false;
        }

    }


    $scope.sembunyikan = function() {
       $scope.tampilkan_code = true;
    };

    $scope.hapus = function(nik) {
        $postData = {'action': 'hapus_karyawan', 'nik': nik};
        $http.post('php/proses.php', $postData).success(function(data) {
            console.log('Karyawan dengan nik ' + nik + ' telah dihapus');
        });
        $location.path('/karyawan');
    };

    $scope.update = function() {
        var nama = $scope.detail_karyawan.nm_karyawan;
        var alamat= $scope.detail_karyawan.alamat;
        var telp = $scope.detail_karyawan.telp
        var msk_krj = $scope.detail_karyawan.msk_krj
        var nik = $scope.detail_karyawan.nik;
        var postData = {'action': 'update_karyawan', 'nik': nik, 'nama': nama, 'alamat': alamat, 'telp': telp, 'msk_krj': msk_krj};
        $http.post('php/proses.php', postData).success(function(data) {
            alert(data.pesan);
        });
    };

    $http.post('php/proses.php', {'action': 'select_jabatan_sub'}).success(function(data) {
        console.log(data);
        $scope.list_jabatan = data;
    });

}]);

payrollApp.controller('tmbhKaryawanCtrl', function($scope, $http, $upload) {

    $postData = {'action': 'select_jabatan_sub'};
    $http.post('php/proses.php', $postData).success(function(data) {
        $scope.select_jabatan_sub = data;
    });

    $postData = {'action': 'get_last_nik'};
    $http.post('php/proses.php', $postData).success(function(data) {
        var nik = parseInt(data.nik) + 1;
        var nikStr = nik.toString();

        $scope.nik_baru = ulang_string('0', 3 - nikStr.length) + nikStr;

    })

    ulang_string = function(str, num) {
        return new Array(parseInt(num) + 1).join(str);
    };

    $scope.tambah = function(karyawan) {
        var data_karyawan = [];
        data_karyawan['action'] = 'tambah_karyawan';
        $scope.data_karyawan = karyawan;
    };
});



//---------------- absensiCtrl -----------

payrollApp.controller('absenCtrl', function($scope, $http) {


    $scope.absen = function(dariJq) {
        $http.post('php/proses.php', dariJq).success(function(data) {
            $scope.balasan = data;
            console.log('data: ' + data);

        });
    };


});

//-----------------------jabatan--------------------

payrollApp.controller('jabatanListCtrl', function($scope, $http, $route) {
    $postData = {action: "list_jabatan"}
    $scope.isLoading = true;
    $http.post('php/proses.php', $postData).success(function(data) {
        $scope.daftar_jabatan = data;
        $scope.isLoading = false;
        $scope.order_by = 'kd_jbt';
    });

    $scope.hapus = function(kd_jbt) {
        $postData = {action: 'hapus_jabatan', kd_jbt: kd_jbt}
        $http.post('php/proses.php', $postData).success(function(data) {
            $route.reload();
        });
    }
});

payrollApp.controller('tmbhJabatanCtrl', function($scope, $http, $route, $location) {
    $postData = {'action': 'get_last_jabatan'};
    $http.post('php/proses.php', $postData).success(function(data) {
        console.log(data)
        var kd = parseInt(data.kode.kode) + 1;
        var kdStr = kd.toString();
        $scope.tes = data


        $scope.kode_baru = ulang_string('0', 2 - kdStr.length) + kdStr;
        $scope.daftar_gol = data.golongan;

    });
    ulang_string = function(str, num) {
        return new Array(parseInt(num) + 1).join(str);
    };


    $scope.simpan = function() {
        $postData = {action: 'tambah_jabatan', kd_jbt: $scope.kode_baru, jabatan: this.jabatan, kd_gol: this.kd_gol, bobot: this.bobot};

        $http.post('php/proses.php', $postData).success(function(data) {
            console.log(data);
            if (data.pesan == 'berhasil') {
                console.log($postData);
                $('#tambahJabatan').modal('hide');

                $route.reload();
            } else {
                alert('data gagal');
            }
        });
    }

});

// ----------------- divisiCtrl ----------------

payrollApp.controller('divisiCtrl', function($http, $scope, $route) {
    var postData = {action: 'list_divisi'};
    $http.post('php/proses.php', postData).success(function(data) {
        $scope.list_divisi = data;
        console.log(data);
    });

    $scope.simpan = function() {
        var tambah = this.nama_divisi;
        $aksi = {action: 'tambah_divisi', divisi: tambah};
        console.log($aksi);
        $http.post('php/proses.php', $aksi).success(function(data) {
            console.log(data);
            $('.modal').modal('hide');
            $route.reload();
        });
    };

    $scope.hapus = function(id_divisi) {
        var postData = {action: 'hapus_divisi', id: id_divisi};
        $http.post('php/proses.php', postData).success(function(data) {
            //console.log(data);
            $route.reload();
            $('#hapus_modal>.modal-body').html(data.pesan)
            $('#hapus_modal').modal('show');
        });
    };

    $scope.get_divisi = function(id_divisi) {
        var postData = {action: 'detail_divisi', id: id_divisi};
        $http.post('php/proses.php', postData).success(function(data) {
            $scope.detail_divisi = data;
            //console.log(data);
        });
    };

    $scope.update = function(id, nm) {
        var postData = {action: 'update_divisi', id: id, divisi: nm}
        $http.post('php/proses.php', postData).success(function(data) {
            console.log(data.pesan);
            $('#editDivisi').modal('hide');
            $route.reload();
        });
    };
});

// komponen gaji

payrollApp.controller('komponenGajiCtrl', function($http, $scope, $location, $route) {
    $scope.isLoading = true;
    $postData = {action: 'list_komponen'};
    $http.post('php/proses.php', $postData).success(function(data) {
        $scope.komponen_gaji = data;
        $scope.isLoading = false;
    });
    $scope.status_komp = {0: "Gaji Pokok", 1: "Tunjangan Tetap", 2: "Tunjangan Tidak tetap", 3: "Potongan"};
    $scope.hapus = function() {
        var kd_komp = this.komponen.kd_komp;
        var postData = {action: 'hapus_komponen', 'kd_komp': kd_komp};
        $http.post('php/proses.php', postData).success(function(data) {
            alert(data.pesan);
            $route.reload();
        });
    }
});


// -------------- tambah gaji ------------------------

payrollApp.controller('tambahKomponenGajiCtrl', function($http, $scope, $location, $route) {
    $postData = {action: 'get_last_komponen'};
    $http.post('php/proses.php', $postData).success(function(data) {
        var kd = parseInt(data.kode) + 1;
        var kdStr = kd.toString();

        $scope.kode = ulang_string('0', 3- kdStr.length) + kdStr;
    });
    ulang_string = function(str, num) {
        return new Array(parseInt(num) + 1).join(str);
    };
    $scope.simpan = function() {
        var kd_komp = this.kode;
        var komponen = this.komponen;
        var ket = this.ket;
        var nilai = $('#nilai').val();
        var status = this.status_komp;
        var postData = {action: 'tambah_komponen', 'kd_komp': kd_komp, 'komponen': komponen, 'ket': ket, 'nilai': nilai, 'status': status};
        $http.post('php/proses.php', postData).success(function(data) {
            console.log(data);
            alert(data.pesan);
            $route.reload();
        });

    };
});

// ------------ absensi -------------------//

payrollApp.controller('absensiCtrl', function($http, $scope) {
    var postData = {'action': 'report_karyawan'};
   $http.post('php/proses.php', postData).success(function(data) {
      $scope.report_karyawan = data;
      $scope.jmlHadir = 0;
      $scope.jmlJam = 0;
      console.log(data);
   });
   $scope.simpan = function() {

    var nik = this.karyawan.nik;
    var jmlh = this.jmlHadir;
    var jam = this.jmlJam;

    var postData = {action: 'simpan_absen', 'nik': nik, 'jmlh': jmlh, 'jam': jam};


    $http.post('php/proses.php', postData).success(function(data) {

        var pesan = data.pesan;
    var jns_pesan = "";
    if(pesan == 'berhasil') {
        jns_pesan = "alert alert-success";
    } else {
        jns_pesan = "alert alert-danger";
    }
    var btn_close = '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    var html = '<div class="' + jns_pesan + '">' + btn_close + pesan + '</div>';
      $('#pesan').hide().html(html).fadeIn('slow');
      console.log(data);
    });
   };
});

//---------------- lembur controller-----------

payrollApp.controller('lemburCtrl', function($http, $scope, $route) {
    var postData = {action: 'data_lembur'}
    $http.post('php/proses.php', postData).success(function(data) {
        $scope.daftar_lembur = data;
        console.log(data);
    });

    $scope.update = function() {
        var postData = {action: 'update_lembur', 'nilai': this.daftar_lembur.nilai}
        $http.post('php/proses.php', postData).success(function(data) {
            console.log(data);
        });
    };
});

//login controller

payrollApp.controller('loginCtrl', function($http, $scope, $location) {

  $scope.login = function() {
    if(this.username == '') {
        $html = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Username masih kosong</strong></div>';
        $('#pesan').html($html);
    } else if(this.password == '') {
         $html = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Password masih kosong</strong></div>';
        $('#pesan').html($html);
    } else {
      $postData = {action: 'login', username: this.username, password: this.password}
      $http.post('php/proses.php', $postData).success(function(data) {
        $scope.pesan = data;
        console.log(data);
        if(data.pesan == 'gagal') {
            $html = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><strong>Username tidak ditemukan atau password salah</strong></div>';
            $('#pesan').html($html);
        } else {
            window.location.reload(true);
        }
    });
   };
  };

  $scope.logout = function() {
    $postData = {action: 'logout'};
    $http.post('php/proses.php', $postData).success(function(data) {
        window.location.reload(true);
    });
  };

});


//toggle qrcode

payrollApp.directive('toggle', function() {
    return function(scope, elem, attrs) {
        scope.$on('event:toggle', function() {
            elem.slideToggle();
        })
    }
});

payrollApp.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                }
            });
        }
    };
});

//---------------------------profile controller -----------------------------//

payrollApp.controller('profileCtrl', function($http, $scope) {
    var postData = {action: 'current_user'};
    $http.post('php/proses.php', postData).success(function(data){
        $scope.user = data.username;

    });
    $scope.update = function() {
        var pass1 = this.pass1;
        var pass2 = this.pass2;
        if(pass1 == "" || pass2 == "") {
            alert("password tidak boleh kosong ");
            return false;
        } else {
            if(pass1 != pass2) {
                alert("kolom password tidak sama");
                return false;
            } else {
                var postData = {action: 'update_user', assword: this.pass1};
                $http.post('php/proses.php', postData).success(function(data) {
                    alert(data.pesan);
                    console.log(data);
                });
            }
        }
    };
});


//------------------------- user controller --------------------------------//

payrollApp.controller('userCtrl', function($http, $scope, $route) {
    var postData = {action: 'daftar_user'}
    $http.post('php/proses.php', postData).success(function(data) {
        $scope.users = data;
    });
    $scope.tambah = function() {
        var username = this.username;
        var pass1 = this.pass1;
        var pass2 = this.pass2;

        if(username=="") {
            alert('Nama Harus di isi');
            return false;
        }
        if(pass1 == "") {
            alert('Password masih kosong');
            return false;
        }

        if(pass2 == "" ) {
            alert('Konfirmasi password kosong');
            return false;
        }

        if(pass1 != pass2) {
            alert("password dan konfirmasi passsword tidak sama");
            return false;
        }

        var postData = {action: 'tambah_user', username: username, password: pass1}
        $http.post('php/proses.php', postData).success(function(data) {
            console.log(data);
            alert(data.pesan);
            $('#modalTambah').modal('hide');
            $route.reload();
        });
    };
    $scope.hapus = function() {
        var postData = {action: 'hapus_user', username: this.user.username};
        $http.post('php/proses.php', postData).success(function(data) {

            $route.reload();
            alert(data.pesan);

        });
    };
});

//--------------------------- Report controller -----------------------------//

payrollApp.controller('reportDftKaryawanCtrl', function($http, $scope, $route) {
   var postData = {'action': 'report_karyawan'};
   $http.post('php/proses.php', postData).success(function(data) {
      $scope.report_karyawan = data;
      console.log(data);
   });
});


//-------------------- pembayaran gaji controller -----------------

payrollApp.controller('bayarGajiCtrl', function($http, $scope, $route) {
    var postData = {'action': 'daftar_absen_karyawan'};

   $http.post('php/proses.php', postData).success(function(data) {
      $scope.daftar_karyawan = data;
      console.log(data);
       $('#hari_kerja').hide();
        $('#jam_lembur').hide();
        $('select').on('change', function() {
            $('#hari_kerja').show();
            $('#jam_lembur').show();
        });
   });
   $http.post('php/proses.php', {action: 'kode_gaji'}).success(function(data) {
        if(data.kd_bayar == null) {
            var kd = 1;
        } else {
            var kd = parseInt(data.kd_bayar) + 1;
        }
        var kdStr = kd.toString();
        $scope.kd_bayar = ulang_string('0', 4 - kdStr.length) + kdStr;
    });
    var tgl = new Date();
    $scope.p_awal = formatDate(tgl);
   $scope.p_akhir = formatDate(tgl)

   $scope.bayar = function() {
        var kd_bayar = this.kd_bayar;
        var nik = this.karyawan.nik;
        var p_awal = $('.p_awal').val();
        var p_akhir = $('.p_akhir').val();
        var postData = {'action': 'bayar_gaji', 'kd_bayar': kd_bayar, 'nik': nik, 'p_awal': p_awal, 'p_akhir': p_akhir};
        $http.post('php/proses.php', postData).success(function(data) {
            console.log(data);
            alert(data.pesan);
            window.location.reload(true);
        });
        console.log(postData);
   };
});

    function ulang_string(str, num) {
        return new Array(parseInt(num) + 1).join(str);
};
 function formatDate(date1) {
  return date1.getFullYear() + '-' +
    (date1.getMonth() < 9 ? '0' : '') + (date1.getMonth()+1) + '-' +
    (date1.getDate() < 10 ? '0' : '') + date1.getDate();
}

//---------------- laporan pembayaran ------------

payrollApp.controller('rptGajiCtrl', function($http, $route, $scope) {
    $scope.tampilkan = function() {
        var p_awal = $('.p_awal').val();
        var p_akhir = $('.p_akhir').val();
        var postData = {action: 'laporan_gaji', 'p_awal': p_awal, 'p_akhir': p_akhir };
        $http.post('php/proses.php', postData).success(function(data) {
            //console.log(data);
            //$route.reload();
            $scope.laporan_gaji = data;
            console.log(data);
            $('#periode').modal('hide');
            $('#link_pdf').attr('href', 'pdf/laporan_penggajian.php?p_awal='+ data[0].data_umum.p_awal + '&p_akhir=' + data[0].data_umum.p_akhir);
        });
    }
});
