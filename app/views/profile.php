<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title">User Profile</h3>
    </div>
    <div class="panel-body">
        <table class="table table-stripped">
            <tr>
                <td class="col-md-3">Username</td>
                <td  class="col-md-3">{{ user }}</td>
            </tr>
            <tr>
                <td>Password</td>
                <td class="col-md-3"><input type="password" class="form-control" ng-model="pass1"></td>
            </tr>
            <tr>
                <td>Password</td>
                <td><input type="password" class="form-control" ng-model="pass2"></td>
            </tr>
             <tr>
                <td colspan="2">
                    Apabila akan mengganti password, isi input password kemudian klik update
                </td>
            </tr>
            <tr>
                <td colspan="2"><button class="btn btn-primary btn-xs" ng-click="update()">Update</button></td>
            </tr>
        </table>
    </div>
</div>
