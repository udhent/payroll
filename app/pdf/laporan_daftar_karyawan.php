<?php
include "../lib/php/DB_Functions.php";
require ('../lib/php/fpdf.php');
date_default_timezone_set('UTC');

if(isset($_GET['p_awal']) and isset($_GET['p_akhir'])) {
    $p_awal = $_GET['p_awal'];
    $p_akhir = $_GET['p_akhir'];
} else {
    $p_awal = '0000-00-00';
    $p_akhir = '0000-00-00';
}


class PDF extends FPDF

//Cell(width, height, txt, border, ln) ln(0 ke kanan, 1 baris baru, 2 bawah)

{

    function page_header($judul) {
        $this->cell(60);
        $this->cell(60, 0, $judul, 'LR');
        $this->Ln(10);
    }

    function tabel($header, $data) {

        //lebar kolom
         $this->SetFont('Arial','',10);
        $w = array(10, 30, 40, 30, 30, 50);

        //header

        for($i=0; $i<count($header); $i++)
            $this->Cell($w[$i], 7, $header[$i], 1, 0, 'C');
        $this->Ln();
        $x = 1;
        foreach($data as $row)
        {
            $this->Cell($w[0],6,$x,'LR');
            $this->Cell($w[1],6,$row['nm_karyawan'],'LR');
            $this->Cell($w[2],6,$row['nm_jbt'],'LR');
            $this->Cell($w[3],6,$row['nm_divisi'],0,0,'C');
            $this->Cell($w[4],6,$row['msk_krj'],'LR');
            $this->Cell($w[5],6,$row['alamat'],'LR');
            $this->Ln();
            $x++;
        }
        $this->Cell(array_sum($w),0,'','T');
        $this->Ln(10);

    }

    function footer() {

    }
}

$pdf = new PDF();
$pdf->SetFont('Arial','',14);
$pdf->AddPage();


$db = new DB_Functions();
$db->connect();


$db->select('karyawan k inner join jabatan j on k.kd_jbt = j.kd_jbt inner join divisi d on k.kd_divisi = d.id', 'k.*, j.nm_jbt, d.nm_divisi');
$pdf->page_header('Laporan Daftar karyawan');
$header = array('NIK', 'Nama Karyawan', 'jabatan', 'Divisi', 'Tgl Masuk', 'Alamat');
$pdf->tabel($header,$db->getResult());
$db->disconnect();




//kolom tabel


// data

$data = $hasil;


$pdf->Output();
