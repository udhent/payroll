
<div>
<div class="modal fade" id="login_modal" style="margin: -300px 0 0 -280px" ng-controller="loginCtrl" data-backdrop="static" data-keyboard="false">
  <div class="modal-body">
    <div class="panel panel-primary">
        <div class="panel-heading">
            Silahkan Login
        </div>
        <div class="panel-body">
          <form class="form-inline">
            <div class="form-group">
                <label for="username">Username</label>
                <input type="text" class="form-control" id="inputUsername" placeholder="username" ng-model="username">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" id="inputPassword" placeholder="password" ng-model="password"/>
            </div>
          </form>
        </div>
        <div class="panel-footer">
            <button class="btn btn-primary" ng-click="login()" data-loading-text="Log you in...">Login <span class="fa fa-sign-in"></span> </button>
        </div>


    </div>
    <div id="pesan"></div>
  </div>
</div>
</div>

<script src="lib/js/jquery/jquery-1.10.2.min.js"></script>
<script src="public/js/bootstrap.min.js"></script>
<script src="public/js/bootstrap-modalmanager.js"></script>
<script src="public/js/bootstrap-modal.js"></script>
<script src="lib/js/jquery/datepick.js"></script>
<script src="lib/js/angular/angular.min.js"></script>
<script src="lib/js/angular/angular-route.min.js"></script>
<script src="lib/js/angular-file-upload.js"></script>
<script src="lib/js/angular/angular-file-upload.min.js"></script>
<script src="lib/js/angular/promise-tracker.js"></script>
<script src="lib/js/qrcode/qrcode_generator.js"></script>
<script src="js/main.js"></script>
<script>

 $(document).ready(function() {
     $('#login_modal').modal({
      show: true,
      backdrop: 'static',
      keyboard: false
     });
 });

</script>
