<div ng-controller="karyawanListCtrl" id="lemburWrap">
    <div class="well well-sm">
        <div class="row">
            <div class="col-xs-8 pull-left">
                <h5>Setup Lembur</h5>
            </div>
            <div class="col-xs-4 pull-right">
                <input ng-model="filter" class="form-control input-sm" placeholder="filter data lembur&hellip;">
            </div>
        </div>
    </div> <!-- well filter -->

    <div ng-bind-html="status_simpan()"></div>
    <table class="table table-striped table-bordered table-hover" id="tabel_lembur">
        <tr class="success">
            <th>Nilai</th>
            <th>&nbsp;</th>
        </tr>
        <tr>
            <td>{{ daftar_lembur.nilai }}</td>
            <td>
              <div class="btn-group btn-group-xs">
                <button class="btn btn-primary" data-toggle="modal" data-target="#editLembur">
                  <span class="fa fa-edit"></span>Edit
                </button>
                <button class="btn btn-default">
                </button>
                <button class="btn btn-danger btn-xs" ng-click="hapus(lembur.kd_lembur)">
                  <span class="glyphicon glyphicon-trash"></span> Hapus
                </button>
              </div>
            </td>
        </tr>
    </table>

    <div ng-show="isLoading">
        <p><img src="../public/images/loading.gif" /> loading ...</p>
    </div>


    <!-- table input -->

</div>

<div class="modal fade" id="editLembur">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Edit Lembur</h4>
    </div>
        <div class="modal-body">
            <table class="table table-bordered">

        <tr>
            <td><input type="text" name="jam_mulai" ng-model="daftar_lembur.nilai" class="form-control input-sm" size="3" placeholder="Nilai"></td>
            <td colspan="5"><button class="btn btn-primary btn-xs" ng-click="update()"><span class="glyphicon glyphicon-save"></span> Simpan</button></td>
        </tr>
    </table>

    </div>
</div>


